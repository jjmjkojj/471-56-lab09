package gooddesigngumball;

public class ChooseState implements State{
	GumballMachine gumballMachine;
	String s;
	
	public ChooseState(GumballMachine gumballMachine,String s){
		this.gumballMachine = gumballMachine;
		this.s = s;
	}
	@Override
	public void insertQuarter() {
		System.out.println("You can't insert another quater");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Quater returned");
		gumballMachine.setState(gumballMachine.getNoQuarterState());
	}

	@Override
	public void turnCrank() {
		System.out.println("You turned...");
		gumballMachine.setState(gumballMachine.getSoldState());
	}

	@Override
	public void dispense() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void choose(String s) {
		System.out.println("You have chosen the flavor "+s);
		gumballMachine.setState(gumballMachine.getChooseState(s));
	}

}
