package gooddesigngumball;

public class SoldOutState implements State {

	GumballMachine gumballMachine;
	
	public SoldOutState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}
	
	public void insertQuarter() {
		System.out.println("You can't insert a quarter, the  machine is sold out");
	}

	public void ejectQuarter() {
		System.out.println("You can't eject, you haven't inserted a quarter yet");
	}

	public void turnCrank() {
		System.out.println("You turned, but there are no gumballs");
	}

	public void dispense() {
		System.out.println("No gumball dispensed");
	}
	
	public String toString() {
		return "Machine is sold out";
	}

	@Override
	public void choose(String s) {
		System.out.println("You have chosen the flavor "+s+", but there are no gumballs");
	}
}
