package gooddesigngumball;

public class HasQuarterState implements State {
	GumballMachine gumballMachine;
	
	public HasQuarterState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}

	public void insertQuarter() {
		System.out.println("You can't insert another quater");
	}

	public void ejectQuarter() {
		System.out.println("Quater returned");
		gumballMachine.setState(gumballMachine.getNoQuarterState());
	}

	public void turnCrank() {
		System.out.println("You have to choose the flavor first");
//		dispense();
//		gumballMachine.setState(gumballMachine.getSoldState());
	}

	public void dispense() {
		System.out.println("No gumball dispensed");
	}

	public String toString() {
		return "Machine is waiting for you to turn the crank";
	}

	@Override
	public void choose(String s) {
		System.out.println("You have chosen the flavor "+s);
		gumballMachine.setState(gumballMachine.getChooseState(s));
	}
}
